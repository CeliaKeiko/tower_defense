﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disparo : MonoBehaviour
{
    [Tooltip("referencia al prefab de la bala")]
    public GameObject bulletprefab;
    public float timeLastShot;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {


        // Instancia una bala cada vez que el jugador pulsa el espacio
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(bulletprefab, transform.position, Quaternion.identity);
            timeLastShot = Time.time;
        }
    }
}
